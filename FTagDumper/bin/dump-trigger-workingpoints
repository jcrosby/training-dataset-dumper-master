#!/usr/bin/env python

"""
Dump some ftag trigger info!

This is the minimal version which doesn't run fullscan tracking
augmentation and association.

As the name implies, this is useful to define working points and do
other simple performance studies.
"""

from FTagDumper import trigger as trig
from FTagDumper import mctc
from FTagDumper import dumper

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

import sys


def get_args():
    default_chain = 'HLT_j20_0eta290_020jvt_pf_ftf_boffperf_L1J15'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('-t','--threads', type=int, default=0)
    parser.add_argument('-n','--chain', default=default_chain, **dh)
    return parser.parse_args()


def trigDatasetDumper(flags, args):
    ca = ComponentAccumulator()

    # load configurations
    combined_cfg = dumper.combinedConfig(args.config_file)
    trig_cfg, dumper_cfg = trig.getJobConfig(combined_cfg)

    # build labeling collections
    labelAlg = trig.getLabelingBuilderAlg(flags)
    ca.addEventAlgo(labelAlg)

    # get jets
    temp_btag, temp_jets = 'tempBtag', 'tempJets'
    ca.merge(trig.triggerJetGetterCfg(
        flags,
        chain=args.chain,
        temp_jets=temp_jets,
        temp_btag=temp_btag,
    ))

    # match to offline jets, pull out some info
    matcher = CompFactory.TriggerBTagMatcherAlg('matcher')
    matcher.offlineBtagKey = 'BTagging_AntiKt4EMTopo'
    matcher.triggerBtagKey = temp_btag
    matcher.floatsToCopy = {
        f'DL1r_p{x}':f'OfflineMatchedDL1r_p{x}' for x in 'bcu'}
    matcher.offlineJetKey = 'AntiKt4EMTopoJets'
    matcher.triggerJetKey = temp_jets
    truth_labels = [
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
    ]
    matcher.jetIntsToCopy = {
        x:f'OfflineMatched{x}' for x in truth_labels}
    ca.addEventAlgo(matcher)

    ca.merge(mctc.getMCTC())
    
    ca.merge(dumper.getDumperConfig(args, config_dict=dumper_cfg))

    return ca


def run():
    args = get_args()


    flags = dumper.update_flags(args)
    flags.Concurrency.NumThreads = args.threads
    if args.threads:
        flags.Concurrency.NumConcurrentEvents = args.threads

    flags.lock()

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = getConfig(flags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # This is also needed for TDT
    ca.merge(MetaDataSvcCfg(flags))

    # Needed to read anything from a file
    ca.merge(PoolReadCfg(flags))

    ca.merge(
        trigDatasetDumper(
            flags,
            args
        )
    )

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
