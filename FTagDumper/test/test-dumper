#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

# config mapping
#
CFG_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../../configs
declare -A CONFIGS=(
    [ufo]=${CFG_DIR}/UFO.json
    [pflow]=${CFG_DIR}/EMPFlow.json
    [pflow]=${CFG_DIR}/EMPFlow.json
    [slim]=${CFG_DIR}/EMPFlowSlim.json
    [truth]=${CFG_DIR}/EMPFlowTruth.json
    [truthjets]=${CFG_DIR}/AktTruthJets.json
    [fatjets]=${CFG_DIR}/FatJets.json
    [hash]=${CFG_DIR}/EMPFlow_hash.json
    [softe]=${CFG_DIR}/EMPFlow_softlep.json
    [minimal]=${CFG_DIR}/minimal.json
    [athena]=${CFG_DIR}/EMPFlow.json
    [retag-old]=${CFG_DIR}/EMPFlowRetagOld.json
    [retag-lite]=${CFG_DIR}/EMPFlowRetag.json
    [retag-mc20]=${CFG_DIR}/pflow-systs/TRK_RES_D0_MEAS.json
    [retag]=${CFG_DIR}/EMPFlowRetagAthena.json
    [retag-minimal]=${CFG_DIR}/EMPFlowRetagMinimal.json
    [retag-varysyst]=${CFG_DIR}/pflow-systs/TRK_RES_D0_MEAS.json
    [genwt]=${CFG_DIR}/EMPFlowGeneratorSystematics.json
    [lrt]=${CFG_DIR}/LRT.json
    [upgrade]=${CFG_DIR}/upgrade_r24.json
    [trackless]=${CFG_DIR}/TracklessEMPFlow.json
    [trigger]=${CFG_DIR}/trigger_pflow.json
    [trigger-emtopo]=${CFG_DIR}/trigger_emtopo.json
    [trigger-all]=${CFG_DIR}/trigger_all.json
    [trigger-mc21]=${CFG_DIR}/trigger_mc21.json
    [trigger-SampleA]=${CFG_DIR}/trigger_all_SampleA.json
    [data]=${CFG_DIR}/EMPFlowData.json
    [multi]=${CFG_DIR}/multi.json
    [flow]=${CFG_DIR}/flow.json
    [trigger-trackjet]=${CFG_DIR}/TriggerTrackJets.json
    [trigger-fatjet]=${CFG_DIR}/TriggerFatJets.json
    [trigger-hits]=${CFG_DIR}/trigger_hits.json
    [jer]=${CFG_DIR}/jetm2_jer.json
    [neutral]=${CFG_DIR}/EMPFlow_all_flows.json
    [retag-fatjet]=${CFG_DIR}/FatJetsRetag.json
    [taucomp]=${CFG_DIR}/TauComp.json
    [blocks]=${CFG_DIR}/BlockDemo.json
)

# standard samples
TRIGGER_MC21_AOD=r13983/AOD.601229.e8453_e8455_s3873_s3874_r13983.pool.root
TRIGGER_AOD=r14634/AOD.601229.e8514_e8528_s4111_s4114_r14634.pool.root
OFFLINE_PHYSVAL=p5627/DAOD_PHYSVAL.410470.e6337_s3681_r13144_p5627.small.pool.root
OFFLINE_FTAG1=p6057/DAOD_FTAG1.601230.e8514_s4162_r14622_p6057.small.pool.root
OFFLINE_FTAG1_MC20=p5981/DAOD_FTAG1.800030.e7954_s3681_r13144_p5981.small.pool.root
OFFLINE_PHYS=p6026/DAOD_PHYS.601229.e8514_e8528_s4162_s4114_r14622_r14663_p6026.small.pool.root

declare -A DATAFILES=(
    [ufo]=${OFFLINE_FTAG1}
    [pflow]=${OFFLINE_FTAG1}
    [slim]=${OFFLINE_FTAG1}
    [truth]=${OFFLINE_FTAG1}
    [truthjets]=${OFFLINE_PHYSVAL}
    [fatjets]=p5488/DAOD_FTAG1.801471.e8441_e7400_s3681_r13144_r13146_p5488.small.pool.root
    [hash]=${OFFLINE_FTAG1}
    [softe]=${OFFLINE_PHYSVAL}
    [minimal]=${OFFLINE_FTAG1}
    [athena]=${OFFLINE_FTAG1}
    [retag-old]=${OFFLINE_FTAG1}
    [retag-lite]=${OFFLINE_FTAG1}
    [retag-mc20]=${OFFLINE_FTAG1_MC20}
    [retag]=${OFFLINE_FTAG1}
    [retag-minimal]=${OFFLINE_FTAG1}
    [retag-varysyst]=${OFFLINE_FTAG1}
    [genwt]=${OFFLINE_FTAG1}
    [lrt]=${OFFLINE_FTAG1}
    [upgrade]=p5799/DAOD_FTAG1.601229.e8481_s4149_r14700_r14702_p5799.small.pool.root
    [trackless]=p6023/DAOD_FTAG1.800030.HITS.e8514_s4159.r15310.p6023.small.root
    [trigger]=${TRIGGER_AOD}
    [trigger-emtopo]=${TRIGGER_AOD}
    [trigger-all]=${TRIGGER_AOD}
    [trigger-mc21]=${TRIGGER_MC21_AOD}
    [trigger-SampleA]=r15183/sampleA.601229.e8514_e8528_s4162_s4114_r15183.root 
    [data]=p5981/DAOD_FTAG2.00349309.r13286_p5981.small.pool.root
    [multi]=${OFFLINE_FTAG1}
    [flow]=${OFFLINE_FTAG1}
    [trigger-trackjet]=${TRIGGER_MC21_AOD}
    [trigger-fatjet]=r13983/AOD.801169.e8453_e8455_s3873_s3874_r13983.pool.root
    [trigger-hits]=${TRIGGER_AOD}
    [jer]=p5548/DAOD_JETM2.e8485_s3986_r14060_p5548.small.pool.root
    [neutral]=${OFFLINE_FTAG1}
    [retag-fatjet]=p5738/DAOD_FTAG1.601229.e8514_s4162_r14622_p5738.small.pool.root
    [taucomp]=${OFFLINE_PHYS}
    [blocks]=${OFFLINE_FTAG1}
)
declare -A TESTS=(
    [ufo]=dump-ufo
    [pflow]=dump-single-btag
    [slim]=dump-single-btag
    [truth]=dump-single-btag
    [truthjets]=dump-single-btag
    [fatjets]=dump-fatjet
    [hash]=dump-single-btag
    [softe]=dump-softe
    [minimal]=dump-minimal-btag
    [athena]=dump-single-btag
    [retag-old]=dump-retag
    [retag-lite]=dump-retag-lite
    [retag-mc20]=dump-retag-lite
    [retag]=dump-retag-lite
    [retag-minimal]=dump-retag-lite
    [retag-varysyst]=dump-retag-lite
    [genwt]=dump-single-btag
    [lrt]=dump-lrt
    [upgrade]=dump-single-btag
    [trackless]=dump-single-btag
    [trigger]=dump-trigger-pflow
    [trigger-emtopo]=dump-trigger-emtopo
    [trigger-all]=dump-trigger-all
    [trigger-mc21]=dump-trigger-all
    [trigger-SampleA]=dump-trigger-all
    [data]=dump-single-btag
    [multi]=dump-multi-config
    [flow]=dump-single-btag
    [trigger-trackjet]=dump-trigger-trackjet
    [trigger-fatjet]=dump-trigger-fatjet
    [trigger-hits]=dump-trigger-emtopo
    [jer]=dump-jer
    [neutral]=dump-single-btag-flow
    [retag-fatjet]=dump-retag-fatjet
    [taucomp]=dump-single-btag
    [blocks]=dump-single-btag
)

################################################
# parse arguments
################################################

ALL_MODES=${!CONFIGS[*]}

print-usage() {
    echo "usage: ${0##*/} [-rpaqh] [-d <dir>] (${ALL_MODES[*]// /|})" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and use it to
produce a training dataset.

Options:
 -d <dir>: specify directory to run in
 -r: build (and run on) a reduced test file
 -p: force dumping at full precision
 -a: enable floating point exception auditing
 -q: just get the input file, don't run the test
 -h: print help

If no -d argument is given we'll create one in /tmp and work there.

EOF
    exit 1
}

DIRECTORY=""
DATA_URL=https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/dumper-test-files/-/raw/main/
REDUCE_INPUT=""
FULL_PRECISION=""
AUDIT_FPE=""
QUIT_AFTER_INPUT_DOWNLOAD=""

while getopts ":d:rpaqh" o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        r) REDUCE_INPUT=1 ;;
        p) FULL_PRECISION="-p" ;;
        a) AUDIT_FPE="--FPEAuditor" ;;
        q) QUIT_AFTER_INPUT_DOWNLOAD=1 ;;
        h) help ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1


############################################
# Check that all the modes / paths exist
############################################
#
if [[ ! ${CONFIGS[$MODE]+x} ]]; then usage; fi
CFG=${CONFIGS[$MODE]}

if [[ ! ${DATAFILES[$MODE]+x} ]]; then usage; fi
DOWNLOAD_PATH=${DATAFILES[$MODE]}
FILE=${DOWNLOAD_PATH##*/}

if [[ ! ${TESTS[$MODE]+x} ]]; then usage; fi
RUN=${TESTS[$MODE]}


############################################
# Define the run command
############################################

# basic run command
function run-test-basic {
    local CMD="$RUN $FILE -c $CFG $FULL_PRECISION $AUDIT_FPE"
    echo "running ${CMD}"
    ${CMD}
}

# run on reduced input file
function run-test-reduced {
    local REDUCED=DAOD_SMALL.pool.root
    local REDCMD="make-test-file $FILE -c $CFG -o $REDUCED "
    echo "running ${REDCMD}"
    ${REDCMD}
    cat <<EOF

#################### successfully ran reduction ####################
now processing $REDUCED to HDF5
####################################################################

EOF
    local CMD="$RUN $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}

if [[ ${REDUCE_INPUT} ]] ; then
    if [[ ${MODE} != "minimal" ]] ; then
        echo "Input reduction is not supported in '${MODE}' mode" >&2
        exit 1
    fi
    RUN_TEST=run-test-reduced
else
    RUN_TEST=run-test-basic
fi


#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

# get files
if [[ ! -f ${FILE} ]] ; then
    echo "getting file ${FILE}" >&2
    curl -s ${DATA_URL}/${DOWNLOAD_PATH} > ${FILE}
fi

if [[ ${QUIT_AFTER_INPUT_DOWNLOAD} ]]; then
    echo "got ${FILE}, quitting"
    exit 0
fi

# now run the test
${RUN_TEST}

# require some jets were written
echo "========== output summary ==========="
test-output output.h5
