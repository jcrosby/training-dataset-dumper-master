#include "src/JetDumperAlg.h"
#include "src/TrackSystematicsAlg.h"
#include "src/TriggerJetGetterAlg.h"
#include "src/TriggerBTagMatcherAlg.h"
#include "src/JetMatcherAlg.h"
#include "src/JetSystematicsAlg.h"
#include "src/MCTCDecoratorAlg.h"
#include "src/TriggerVRJetOverlapDecoratorTool.h"
#include "src/ParentLinkDecoratorAlg.h"
#include "src/TruthJetPrimaryVertexDecoratorAlg.h"
#include "src/TrackFlowOverlapRemovalAlg.h"
#include "src/FlowSelectorAlg.h"
#include "src/HitELDecoratorAlg.h"
#include "src/HitdRMinDecoratorAlg.h"


DECLARE_COMPONENT(JetDumperAlg)
DECLARE_COMPONENT(HitELDecoratorAlg)
DECLARE_COMPONENT(HitdRMinDecoratorAlg)
DECLARE_COMPONENT(TrackSystematicsAlg)
DECLARE_COMPONENT(TriggerJetGetterAlg)
DECLARE_COMPONENT(TriggerBTagMatcherAlg)
DECLARE_COMPONENT(JetMatcherAlg)
DECLARE_COMPONENT(JetSystematicsAlg)
DECLARE_COMPONENT(MCTCDecoratorAlg)
DECLARE_COMPONENT(TriggerVRJetOverlapDecoratorTool)
DECLARE_COMPONENT(ParentLinkDecoratorAlg)
DECLARE_COMPONENT(TruthJetPrimaryVertexDecoratorAlg)
DECLARE_COMPONENT(TrackFlowOverlapRemovalAlg)
DECLARE_COMPONENT(FlowSelectorAlg)
