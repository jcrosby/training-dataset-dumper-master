#ifndef TRUTH_SUMMARY_DECORATOR_HH
#define TRUTH_SUMMARY_DECORATOR_HH

#include "xAODBTagging/BTaggingContainerFwd.h"
#include "xAODJet/JetContainerFwd.h"
#include "xAODTruth/TruthParticleFwd.h"

#include "AthLinks/ElementLink.h"


class JetTruthSummaryDecorator {
 public:
  using AE = SG::AuxElement;
  typedef ElementLink<xAOD::IParticleContainer> PartLink;
  typedef std::vector<PartLink> PartLinks;

  JetTruthSummaryDecorator(const std::string& link_name, const std::string& out_name);

  float getMinimumDeltaR(const xAOD::Jet& jet, const PartLinks& part_links) const;
  void decorate(const xAOD::Jet& jet) const;

 private:
  AE::ConstAccessor<PartLinks> m_truth_particle_links;
  AE::Decorator<int> m_deco_n_truth;
  AE::Decorator<float> m_deco_min_dr;
};
#endif
